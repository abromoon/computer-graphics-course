import { initBuffers } from './javascript/webgl-init.js'
import { drawScene } from './javascript/webgl-render.js'
import { vertexShader, fragmentShader } from './javascript/weblg-shader.js'


const canvas = document.getElementById('webgl-canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const gl = canvas.getContext('webgl');
if (!gl) {
  console.error('Unable to initialize WebGL. Your browser may not support it.');
}

const shaderProgram = gl.createProgram();

gl.attachShader(shaderProgram, vertexShader);
gl.attachShader(shaderProgram, fragmentShader);
gl.linkProgram(shaderProgram);

if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
  console.error('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
}

const programInfo = {
  program: shaderProgram,
  attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor'),
  },
  uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
  },
};

const buffers = initBuffers(gl);
let angle = 0;
let then = 0;

function render(now) {
  now *= 0.001;
  const deltaTime = now - then;
  then = now;

  angle += deltaTime;

  drawScene(gl, programInfo, buffers, angle);

  requestAnimationFrame(render);
}

requestAnimationFrame(render);