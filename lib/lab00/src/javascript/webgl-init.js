export const vertices = [
  // Front face
  -1.0, -1.0,  1.0,
   1.0, -1.0,  1.0,
   1.0,  1.0,  1.0,
  -1.0,  1.0,  1.0,
  // Back face
  -1.0, -1.0, -1.0,
   1.0, -1.0, -1.0,
   1.0,  1.0, -1.0,
  -1.0,  1.0, -1.0,
  // Top face
  -1.0,  1.0, -1.0,
   1.0,  1.0, -1.0,
   1.0,  1.0,  1.0,
  -1.0,  1.0,  1.0,
  // Bottom face
  -1.0, -1.0, -1.0,
   1.0, -1.0, -1.0,
   1.0, -1.0,  1.0,
  -1.0, -1.0,  1.0,
  // Right face
   1.0, -1.0, -1.0,
   1.0,  1.0, -1.0,
   1.0,  1.0,  1.0,
   1.0, -1.0,  1.0,
  // Left face
  -1.0, -1.0, -1.0,
  -1.0,  1.0, -1.0,
  -1.0,  1.0,  1.0,
  -1.0, -1.0,  1.0,
];

export const indices = [
  0,  1,  2,      0,  2,  3,    // front
  4,  5,  6,      4,  6,  7,    // back
  8,  9,  10,     8,  10, 11,   // top
  12, 13, 14,     12, 14, 15,   // bottom
  16, 17, 18,     16, 18, 19,   // right
  20, 21, 22,     20, 22, 23    // left
];

const colors = [
  // Front face
  1.0, 0.0, 0.0, 0.6,    // red
  1.0, 0.0, 0.0, 0.6,    // red
  1.0, 0.0, 0.0, 0.6,    // red
  1.0, 0.0, 0.0, 0.6,    // red
  // Back face
  0.0, 1.0, 0.0, 0.6,    // green
  0.0, 1.0, 0.0, 0.6,    // green
  0.0, 1.0, 0.0, 0.6,    // green
  0.0, 1.0, 0.0, 0.6,    // green
  // Top face
  0.0, 0.0, 1.0, 0.6,    // blue
  0.0, 0.0, 1.0, 0.6,    // blue
  0.0, 0.0, 1.0, 0.6,    // blue
  0.0, 0.0, 1.0, 0.6,    // blue
  // Bottom face
  1.0, 1.0, 0.0, 0.6,    // yellow
  1.0, 1.0, 0.0, 0.6,    // yellow
  1.0, 1.0, 0.0, 0.6,    // yellow
  1.0, 1.0, 0.0, 0.6,    // yellow
  // Right face
  1.0, 0.0, 1.0, 0.6,    // purple
  1.0, 0.0, 1.0, 0.6,    // purple
  1.0, 0.0, 1.0, 0.6,    // purple
  1.0, 0.0, 1.0, 0.6,    // purple
  // Left face
  0.0, 1.0, 1.0, 0.6,    // cyan
  0.0, 1.0, 1.0, 0.6,    // cyan
  0.0, 1.0, 1.0, 0.6,    // cyan
  0.0, 1.0, 1.0, 0.6,    // cyan
];

export function initBuffers(gl) {
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  const colorBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

  return {
    position: positionBuffer,
    indices: indexBuffer,
    color: colorBuffer
  };
}

// // Создаем массив для хранения ссылок на загруженные текстуры
// const textures = [];

// // Функция для загрузки изображений и создания текстур
// function loadTexture(gl, url, index) {
//     const texture = gl.createTexture();
//     gl.bindTexture(gl.TEXTURE_2D, texture);

//     // Опции для текстуры
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

//     const image = new Image();
//     image.onload = function() {
//         gl.bindTexture(gl.TEXTURE_2D, texture);
//         gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
//         textures[index] = texture;
//     };
//     image.src = url;
// }

// // Загружаем изображения и создаем текстуры
// const imageUrls = [
//   '../images/floppa_face.jpg', 
//   '../images/floppa_back.jpg', 
//   '../images/floppa_top.jpg', 
//   '../images/floppa_bot.jpg', 
//   '../images/floppa_l.jpg', 
//   '../images/floppa_r.jpg',
// ]

// imageUrls.forEach((url, index) => {
//     loadTexture(gl, url, index);
// })