// Функции для работы с матрицами
export function mat4_create() {
  return new Float32Array([
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1
  ]);
}

export function mat4_translate(matrix, vec) {
  matrix[12] += vec[0];
  matrix[13] += vec[1];
  matrix[14] += vec[2];
}

export function mat4_rotate(matrix, angle, axis) {
  const x = axis[0], y = axis[1], z = axis[2];
  const len = Math.sqrt(x * x + y * y + z * z);
  if (len === 0) return;
  const s = Math.sin(angle);
  const c = Math.cos(angle);
  const t = 1 - c;

  const a00 = matrix[0], a01 = matrix[1], a02 = matrix[2], a03 = matrix[3];
  const a10 = matrix[4], a11 = matrix[5], a12 = matrix[6], a13 = matrix[7];
  const a20 = matrix[8], a21 = matrix[9], a22 = matrix[10], a23 = matrix[11];

  const b00 = x * x * t + c, b01 = y * x * t + z * s, b02 = z * x * t - y * s;
  const b10 = x * y * t - z * s, b11 = y * y * t + c, b12 = z * y * t + x * s;
  const b20 = x * z * t + y * s, b21 = y * z * t - x * s, b22 = z * z * t + c;

  matrix[0] = a00 * b00 + a10 * b01 + a20 * b02;
  matrix[1] = a01 * b00 + a11 * b01 + a21 * b02;
  matrix[2] = a02 * b00 + a12 * b01 + a22 * b02;
  matrix[3] = a03 * b00 + a13 * b01 + a23 * b02;
  matrix[4] = a00 * b10 + a10 * b11 + a20 * b12;
  matrix[5] = a01 * b10 + a11 * b11 + a21 * b12;
  matrix[6] = a02 * b10 + a12 * b11 + a22 * b12;
  matrix[7] = a03 * b10 + a13 * b11 + a23 * b12;
  matrix[8] = a00 * b20 + a10 * b21 + a20 * b22;
  matrix[9] = a01 * b20 + a11 * b21 + a21 * b22;
  matrix[10] = a02 * b20 + a12 * b21 + a22 * b22;
  matrix[11] = a03 * b20 + a13 * b21 + a23 * b22;
}

export function mat4_perspective(out, fov, aspect, near, far) {
  const f = 1.0 / Math.tan(fov / 2);
  const nf = 1 / (near - far);
  out[0] = f / aspect;
  out[1] = 0;
  out[2] = 0;
  out[3] = 0;
  out[4] = 0;
  out[5] = f;
  out[6] = 0;
  out[7] = 0;
  out[8] = 0;
  out[9] = 0;
  out[10] = (far + near) * nf;
  out[11] = -1;
  out[12] = 0;
  out[13] = 0;
  out[14] = (2 * far * near) * nf;
  out[15] = 0;
  return out;
}