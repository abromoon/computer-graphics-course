function initBuffers(gl) {

   const scale = 0.3;
   const ratio = gl.canvas.clientHeight / gl.canvas.clientWidth;

   const sqPosBuf = [
      -1,-1,  -1,1,  -3,-1,  -3,1,
   ];
   const trPosBuf = [
      2,1,  1,-1,  3, -1,
   ];

   const sqColorBuf = [
      1,1,0,  1,1,0,  1,1,0,  1,1,0,  // yellow
   ];
   const trColorBuf = [
      0,1,0,  0,0,1,  1,0,0,
   ];

   for (let i = 0; i < sqPosBuf.length; i++) {
      if (i%2)
         sqPosBuf[i] *= scale / ratio;
      else
         sqPosBuf[i] *= scale;
   }
   for (let i = 0; i < trPosBuf.length; i++) {
      if (i%2)
         trPosBuf[i] *= scale / ratio;
      else
         trPosBuf[i] *= scale;
   }

   return [initBuffer(gl, sqPosBuf, sqColorBuf), initBuffer(gl, trPosBuf, trColorBuf)];
}

function initBuffer(gl, pos, color) {
   const positionBuffer = initPositionBuffer(gl, pos);
   const colorBuffer = initColorBuffer(gl, color);

   return {
      position: positionBuffer,
      color: colorBuffer,
   };
}

function initPositionBuffer(gl, positions) {
   const positionBuffer = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
   gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

   return positionBuffer;
}

function initColorBuffer(gl, colors) {
   const colorBuffer = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
   gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

   return colorBuffer;
}

export { initBuffers };
