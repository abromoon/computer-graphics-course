import { mat4 } from '../../node_modules/gl-matrix/esm/index.js';

// Canvas setup
const canvas = document.getElementById('webgl-canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const gl = canvas.getContext('webgl2');

// Buffers setup
const cubeVertices = [
  -1,-1,-1,     1,-1,-1,     1, 1,-1,    -1, 1,-1,  // back
  -1,-1, 1,     1,-1, 1,     1, 1, 1,    -1, 1, 1,  // front
  -1,-1,-1,    -1, 1,-1,    -1, 1, 1,    -1,-1, 1,  // left
   1,-1,-1,     1, 1,-1,     1, 1, 1,     1,-1, 1,  // right
  -1,-1,-1,    -1,-1, 1,     1,-1, 1,     1,-1,-1,  // bottom
  -1, 1,-1,    -1, 1, 1,     1, 1, 1,     1, 1,-1,  // top
];

const cubeColors = [
  [1, 0, 0, 1], // red
  [0, 1, 0, 1], // green
  [0, 0, 1, 1], // blue
  [1, 1, 0, 1]  // yellow
];

const cubeIndices = [
   0,  1,  2,    0,  2,  3,
   4,  5,  6,    4,  6,  7,
   8,  9, 10,    8, 10, 11,
  12, 13, 14,   12, 14, 15,
  16, 17, 18,   16, 18, 19,
  20, 21, 22,   20, 22, 23,
];

// Filling the buffers with data
const vertexBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertices), gl.STATIC_DRAW);

const indexBuffer = gl.createBuffer();
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);


// Shaders
const vsSource = `#version 300 es
    precision mediump float;

    in vec4 aVertexPosition;

    uniform vec4 uCubeColor;
    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;
    uniform mat4 uMoveMatrix;
    uniform mat4 uGroupRotateMatrix;

    out vec4 vColor;
    out vec4 vPosition;

    void main(void) {
        gl_Position = uProjectionMatrix * uModelViewMatrix * uMoveMatrix * uGroupRotateMatrix * aVertexPosition;
        vColor = uCubeColor;
        vPosition = aVertexPosition;
    }
`;

const fsSource = `#version 300 es
    precision mediump float;
    in vec4 vColor;
    out vec4 color;

    void main() {
        color = vColor;
    }
`;

// Loading a shader program
const vertShader = gl.createShader(gl.VERTEX_SHADER);
gl.shaderSource(vertShader, vsSource);
gl.compileShader(vertShader);

const fragShader = gl.createShader(gl.FRAGMENT_SHADER);
gl.shaderSource(fragShader, fsSource);
gl.compileShader(fragShader);

const shaderProgram = gl.createProgram();
gl.attachShader(shaderProgram, vertShader);
gl.attachShader(shaderProgram, fragShader);
gl.linkProgram(shaderProgram);

// Associating attributes to vertex shader
const projectionMatrixLocation = gl.getUniformLocation(shaderProgram, "uProjectionMatrix");
const modelViewMatrixLocation = gl.getUniformLocation(shaderProgram, "uModelViewMatrix");
const moveMatrixLocation = gl.getUniformLocation(shaderProgram, "uMoveMatrix");
const groupRotateMatrixLocation = gl.getUniformLocation(shaderProgram, "uGroupRotateMatrix");
const cubeColorLocation = gl.getUniformLocation(shaderProgram, "uCubeColor");

// Linking vertex buffer
gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
const vertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
gl.vertexAttribPointer(vertexPosition, 3, gl.FLOAT, false, 0, 0);
gl.enableVertexAttribArray(vertexPosition);

gl.useProgram(shaderProgram);

// Matrices buffer
const FoV = (75 * Math.PI) / 180;
const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
const zNear = 0.5;
const zFar = 100.0;
const projectionMatrix = mat4.create();

mat4.perspective(projectionMatrix, FoV, aspect, zNear, zFar);

const modelViewMatrix = mat4.create();

const pivot = [0, -4, -10.0];
const negPivot = pivot.map(function (x) { return -x; });

mat4.translate(
  projectionMatrix,
  projectionMatrix,
  pivot,
);

const rotationRate = 0.07;

const podiumRotMatrices = [
  mat4.create(), mat4.create(), mat4.create(), mat4.create()
];

const individualCubeRotMatrices = [
  mat4.create(), mat4.create(), mat4.create(), mat4.create()
];

const offsets = [
  [-2, 0, 0],  // Первый куб
  [ 4, 0, 0],  // Второй куб
  [-2, 2, 0],  // Третий куб
  [ 0,-2, 0],  // Четвертый куб
];

// Keys event listener setup
window.addEventListener(
  "keydown",
  (event) => {
    switch (event.key) {
      case "w":  // поворот всего пьедестала вокруг оси, проходящей через его центр
        mat4.translate(podiumRotMatrices[1], podiumRotMatrices[1], [2, 0, 0]);
        mat4.rotateY(podiumRotMatrices[1], podiumRotMatrices[1], rotationRate);
        mat4.translate(podiumRotMatrices[1], podiumRotMatrices[1], [-2, 0, 0]);

        mat4.rotateY(podiumRotMatrices[0], podiumRotMatrices[0], rotationRate);
        mat4.rotateY(podiumRotMatrices[3], podiumRotMatrices[3], rotationRate);

        mat4.translate(podiumRotMatrices[2], podiumRotMatrices[2], [-2, 0, 0]);
        mat4.rotateY(podiumRotMatrices[2], podiumRotMatrices[2], rotationRate);
        mat4.translate(podiumRotMatrices[2], podiumRotMatrices[2], [2, 0, 0]);

        break;
      case "s":  // поворот всего пьедестала вокруг оси, проходящей через его центр
        mat4.translate(podiumRotMatrices[1], podiumRotMatrices[1], [2, 0, 0]);
        mat4.rotateY(podiumRotMatrices[1], podiumRotMatrices[1], -rotationRate);
        mat4.translate(podiumRotMatrices[1], podiumRotMatrices[1], [-2, 0, 0]);


        mat4.rotateY(podiumRotMatrices[0], podiumRotMatrices[0], -rotationRate);
        mat4.rotateY(podiumRotMatrices[3], podiumRotMatrices[3], -rotationRate);

        mat4.translate(podiumRotMatrices[2], podiumRotMatrices[2], [-2, 0, 0]);
        mat4.rotateY(podiumRotMatrices[2], podiumRotMatrices[2], -rotationRate);
        mat4.translate(podiumRotMatrices[2], podiumRotMatrices[2], [2, 0, 0]);

        break;
      case "D":  // поворот всего пьедестала вокруг оси Y в глобальной системе координат
        mat4.translate(modelViewMatrix, modelViewMatrix, negPivot);
        mat4.rotateY(modelViewMatrix, modelViewMatrix, rotationRate);
        mat4.translate(modelViewMatrix, modelViewMatrix, pivot);

        break;
      case "A":  // поворот всего пьедестала вокруг оси Y в глобальной системе координат
        mat4.translate(modelViewMatrix, modelViewMatrix, negPivot);
        mat4.rotateY(modelViewMatrix, modelViewMatrix, -rotationRate);
        mat4.translate(modelViewMatrix, modelViewMatrix, pivot);

        break;
      case "d":  // поворот каждого кубика вокруг оси, проходящей через его центр
        mat4.rotateY(individualCubeRotMatrices[0], individualCubeRotMatrices[0], rotationRate);
        mat4.rotateY(individualCubeRotMatrices[1], individualCubeRotMatrices[1], rotationRate);
        mat4.rotateY(individualCubeRotMatrices[2], individualCubeRotMatrices[2], rotationRate);
        mat4.rotateY(individualCubeRotMatrices[3], individualCubeRotMatrices[3], rotationRate);

        break;
      case "a":  // поворот каждого кубика вокруг оси, проходящей через его центр
        mat4.rotateY(individualCubeRotMatrices[0], individualCubeRotMatrices[0], -rotationRate);
        mat4.rotateY(individualCubeRotMatrices[1], individualCubeRotMatrices[1], -rotationRate);
        mat4.rotateY(individualCubeRotMatrices[2], individualCubeRotMatrices[2], -rotationRate);
        mat4.rotateY(individualCubeRotMatrices[3], individualCubeRotMatrices[3], -rotationRate);

        break;
      default:
        return;
    }

    event.preventDefault();
  });

function animate() {
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);
  gl.clearColor(0.25, 0.25, 0.25, 1);
  gl.clearDepth(1.0);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.uniformMatrix4fv(projectionMatrixLocation, false, projectionMatrix);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

  for (let i = 0; i < offsets.length; i++) {
    gl.uniformMatrix4fv(moveMatrixLocation, false, podiumRotMatrices[i]);
    gl.uniformMatrix4fv(modelViewMatrixLocation, false, modelViewMatrix);
    gl.uniformMatrix4fv(groupRotateMatrixLocation, false, individualCubeRotMatrices[i]);

    gl.uniform4f(cubeColorLocation, ...cubeColors[i]);
    gl.drawElements(gl.TRIANGLES, cubeIndices.length, gl.UNSIGNED_SHORT, 0);
    mat4.translate(modelViewMatrix, modelViewMatrix, offsets[i]);
  }

  window.requestAnimationFrame(animate);
}

animate();
