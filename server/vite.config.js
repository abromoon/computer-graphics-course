import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    rollupOptions: {
      input: {
        app: './lab00/index.html'
      }
    }
  },
  server: {
    open: './lab00/index.html'
  }
})